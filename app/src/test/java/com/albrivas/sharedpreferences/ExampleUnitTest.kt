/*
 * File: ExampleUnitTest.kt
 * Project: sharedpreferences
 *
 * Created by albrivas on 21/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 */

package com.albrivas.sharedpreferences

import org.junit.Test

import org.junit.Assert.*

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}

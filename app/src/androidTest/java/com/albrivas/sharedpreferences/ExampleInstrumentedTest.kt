/*
 * File: ExampleInstrumentedTest.kt
 * Project: sharedpreferences
 *
 * Created by albrivas on 21/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 */

package com.albrivas.sharedpreferences

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getTargetContext()
        assertEquals("com.albrivas.sharedpreferences", appContext.packageName)
    }
}

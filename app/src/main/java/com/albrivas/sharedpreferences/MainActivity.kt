/*
 * File: MainActivity.kt
 * Project: sharedpreferences
 *
 * Created by albrivas on 21/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 */

package com.albrivas.sharedpreferences

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.albrivas.sharedpreferences.app.preferences
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /*preferences.apply {
            name = "Alberto"
            age = 24
        }*/

        actions()
        getValuePreferences()
    }

    private fun actions() {
        buttonSave.setOnClickListener {
            setValuePreferences()
            cleanEditText()
            getValuePreferences()}
    }

    private fun getValuePreferences() {
        if(preferences.name.isNotEmpty() && preferences.age >= 0) {
            text.text = "Welcome ${preferences.name} , your age is ${preferences.age}"
        } else {
            text.text = "Welcome. Please save your name and your age"
        }
    }

    private fun setValuePreferences() {
        if(textName.text.toString().isNotEmpty() && textAge.text.toString().isNotEmpty() && textLasname.text.toString().isNotEmpty()) {
            preferences.apply {
                name = textName.text.toString()
                age = textAge.text.toString().toInt()
                lastName = textLasname.text.toString()
                alert("Value have been save successfully ", "Alert").show()
            }
        } else {
            alert("Please fill the name and the age before saving", "Alert")
        }
    }

    private fun cleanEditText() {
        textName.text.clear()
        textLasname.text.clear()
        textAge.text.clear()
    }
}

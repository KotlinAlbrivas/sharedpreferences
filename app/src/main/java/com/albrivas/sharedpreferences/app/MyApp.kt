/*
 * File: MyApp.kt
 * Project: sharedpreferences
 *
 * Created by albrivas on 21/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 */

package com.albrivas.sharedpreferences.app

import android.app.Application
import com.albrivas.sharedpreferences.others.MySharedPreferences

// lazy -> Nunca se ejecuta si nadie accede a esa variable
val preferences: MySharedPreferences  by lazy {
    MyApp.prefs!!
}

class MyApp: Application() {
    companion object {
        var prefs: MySharedPreferences? = null
    }

    override fun onCreate() {
        super.onCreate()
        prefs = MySharedPreferences(applicationContext)
    }
}
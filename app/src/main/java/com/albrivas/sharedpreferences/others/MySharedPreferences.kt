/*
 * File: MySharedPreferences.kt
 * Project: sharedpreferences
 *
 * Created by albrivas on 21/06/2019
 * Copyright © 2019 albrivas. All rights reserved.
 */

package com.albrivas.sharedpreferences.others

import android.content.Context

class MySharedPreferences(context: Context) {

    private val fileName = "mis_preferencias"

    // MODE_PRIVATE: para que una app de terceros pueda acceder a nuetros shared preferences
    private val prefs  = context.getSharedPreferences(fileName, Context.MODE_PRIVATE)

    // Variables a guardar en el fichero
    var name: String
        get() = prefs.getString("name", "")
        set(value) = prefs.edit().putString("name", value).apply()

    var lastName: String
        get() = prefs.getString("lastName", "")
        set(value) = prefs.edit().putString("lastName", value).apply()

    var age: Int
        get() = prefs.getInt("age", -1)
        set(value) = prefs.edit().putInt("age", value).apply()


}